import json
import zmq
import sys


def client():
    f = open("host", "r")
    host = f.read().strip()
    f.close()

    context = zmq.Context()
    socket = context.socket(zmq.PAIR)
    socket.connect(host)
    print("[ CLIENT ] Running in host: "+host)

    poller = zmq.Poller()
    poller.register(sys.stdin, zmq.POLLIN)
    poller.register(socket, zmq.POLLIN)

    while True:
        socks = dict(poller.poll())
        if sys.stdin.fileno() in socks and socks[sys.stdin.fileno()] == zmq.POLLIN:
            message = sys.stdin.readline().strip()
            socket.send(str.encode(message))
        if socket in socks and socks[socket] == zmq.POLLIN:
            message = socket.recv()
            try:
                print(message.decode())
#                parsed = json.loads(message.decode())
#                print(json.dumps(parsed, indent=2))
            except ValueError:
                print(message.decode())


client()
