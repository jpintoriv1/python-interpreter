# Interpreter-py

It's a simple server-client for run python code. Made to practice zmq and docker.
Have 2 principals folder client and server. 

## Installation

For install the server you need to have installed docker, python3 and pip. After you only run:

```bash
chmod 755 run.sh
./bash.sh
```

For "install" the client, it is suggested to create and activate a virtual environment, install the requirements.txt and after run the client/client.py file:

```bash
virtualenv env
source env/bin/activate
pip install -r requirements.txt
python client/client.py
```

## Usage
*The nexts commands are used through the client*

Send an ***exec****:

```
{"channel":"RUN", "payload":{"code":"print(\"hola\")"}}
```
response:
```
{"channel": "EXECUTED", "payload": {"result": ["hola"], "result_type": ["String"], "exec_no": [1]}}
```
---
***Heartbeat:***
Expects a BEAT response immediately

```
HEARTBEAT
```
response:
```
BEAT
```
---
***Kill***:
Immediately kills the service

```
KILL
```
response:
```
KILLED
```

**If you want to launch the service again:**
```bash
./run.sh
```
---
***Introspection***
```
{"channel":"RUN", "payload":{"code":"import math"}}
{"channel":"INTROSPECTION", "payload":{"query":"math.","limit":5}}
```
response:
```
{"channel": "INTROSPECTION", "payload": {"query": "math.", "result": ["acos", "acosh", "asin", "asinh", "atan"]}}
```
---
**Managing errors**
Simply print the error and send a message with the error and the traceback
```
{"channel":"RUN", "payload":{"code":"sqrt(9)"}}
```
response:
```
{"channel": "EXECUTED", "payload": {"result": ["name 'sqrt' is not defined"], "result_type": ["error"], "exec_no": [1], "traceback": ["  File \"<string>\", line 1, in <module>", "NameError: name 'sqrt' is not defined", ""]}}
```
You cannot import a library that could be risk for the system
```
{"channel":"RUN", "payload":{"code":"import os"}}
```
response:
```
{"channel": "EXECUTED", "payload": {"result": ["Error in import \n Not permitted import module os"], "result_type": ["error"], "exec_no": [3], "traceback": ["ValueError: Error in import ", " Not permitted import module os", ""]}}
```
---

## Examples

```
{"channel":"RUN", "payload":{"code":"print(\"hola\")"}}
{"channel":"RUN", "payload":{"code":"a=10"}}
{"channel":"RUN", "payload":{"code":"time.sleep(5)"}}
{"channel":"RUN", "payload":{"code":"print(a)"}}
{"channel":"RUN", "payload":{"code":"a=a+1"}}
{"channel":"RUN", "payload":{"code":"print(a)"}}
```
response:
```
{"channel": "EXECUTED", "payload": {"result": ["hola"], "result_type": ["<class 'str'>"], "exec_no": [1]}}
{"channel": "EXECUTED", "payload": {"result": [""], "result_type": ["None"], "exec_no": [2]}}
{"channel": "EXECUTED", "payload": {"result": ["name 'time' is not defined"], "result_type": ["error"], "exec_no": [3], "traceback": ["  File \"<string>\", line 1, in <module>", "NameError: name 'time' is not defined", ""]}}
{"channel": "EXECUTED", "payload": {"result": ["10"], "result_type": ["<class 'int'>"], "exec_no": [4]}}
{"channel": "EXECUTED", "payload": {"result": [""], "result_type": ["None"], "exec_no": [5]}}
{"channel": "EXECUTED", "payload": {"result": ["11"], "result_type": ["<class 'int'>"], "exec_no": [6]}}
```
---
```
{"channel":"RUN", "payload":{"code":"[print(item) for item in [1, 2, 3]]"}}
```
response:
```
{"channel": "EXECUTED", "payload": {"result": ["1", "2", "3"], "result_type": ["<class 'int'>", "<class 'int'>", "<class 'int'>"], "exec_no": [1]}}
```
---
```
{"channel":"RUN", "payload":{"code":"[print(item) for item in [\"A\", 2, 3.5, True]]"}}
```
response:
```
{"channel": "EXECUTED", "payload": {"result": ["A", "2", "3.5", "True"], "result_type": ["<class 'str'>", "<class 'int'>", "<class 'float'>", "<class 'bool'>"], "exec_no": [1]}}
```
---
```
{"channel":"RUN", "payload":{"code":"print(\"hola\")"}}
{"channel":"RUN", "payload":{"code":"a=10"}}
{"channel":"RUN", "payload":{"code":"import time"}}
{"channel":"RUN", "payload":{"code":"time.sleep(10)"}}
{"channel":"RUN", "payload":{"code":"print(\"HOLA\")"}}
KILL
```
response:
```
{"channel": "EXECUTED", "payload": {"result": ["hola"], "result_type": ["<class 'str'>"], "exec_no": [1]}}
{"channel": "EXECUTED", "payload": {"result": [""], "result_type": ["None"], "exec_no": [2]}}
{"channel": "EXECUTED", "payload": {"result": [""], "result_type": ["None"], "exec_no": [3]}}
KILLED!
```
Note that the last print payload doesn't run, because KILL kills the service.

---
```
{"channel":"RUN", "payload":{"code":"sqrt(9)"}}
{"channel":"RUN", "payload":{"code":"import math"}}
{"channel":"RUN", "payload":{"code":"print(math.sqrt(9))"}}
```
response:
```
{"channel": "EXECUTED", "payload": {"result": ["name 'sqrt' is not defined"], "result_type": ["error"], "exec_no": [1], "traceback": ["  File \"<string>\", line 1, in <module>", "NameError: name 'sqrt' is not defined", ""]}}
{"channel": "EXECUTED", "payload": {"result": [""], "result_type": ["None"], "exec_no": [2]}}
{"channel": "EXECUTED", "payload": {"result": ["3.0"], "result_type": ["<class 'int'>"], "exec_no": [3]}}
```
---
```
{"channel":"RUN", "payload":{"code":"print({\"test_dict\":2})"}}
{"channel":"RUN", "payload":{"code":"print([1,2,3,4])"}}
{"channel":"RUN", "payload":{"code":"print((1,2,4,5))"}}
```
response:
```
{"channel": "EXECUTED", "payload": {"result": ["{'test_dict': 2}"], "result_type": ["<class 'dict'>"], "exec_no": [1]}}
{"channel": "EXECUTED", "payload": {"result": ["[1, 2, 3, 4]"], "result_type": ["<class 'list'>"], "exec_no": [2]}}
{"channel": "EXECUTED", "payload": {"result": ["(1, 2, 4, 5)"], "result_type": ["<class 'tuple'>"], "exec_no": [3]}}
```
---
```
{"channel":"INTROSPECTION", "payload":{"query":"math."}}
{"channel":"RUN", "payload":{"code":"import math"}}
{"channel":"INTROSPECTION", "payload":{"query":"math."}}
{"channel":"INTROSPECTION", "payload":{"query":"math.","limit":5}}
```
response:
```
{"channel": "INTROSPECTION", "payload": {"query": "math.", "result": []}}
{"channel": "EXECUTED", "payload": {"result": [""], "result_type": ["None"], "exec_no": [1]}}
{"channel": "INTROSPECTION", "payload": {"query": "math.", "result": ["acos", "acosh", "asin", "asinh", "atan", "atan2", "atanh", "ceil", "copysign", "cos", "cosh", "degrees", "e", "erf", "erfc", "exp", "expm1", "fabs", "factorial", "floor"]}}
{"channel": "INTROSPECTION", "payload": {"query": "math.", "result": ["acos", "acosh", "asin", "asinh", "atan"]}}
```
---
