FROM python:3.8-slim-buster

WORKDIR ./project

#Virtualenv
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# Install dependencies:
COPY ./requirements.txt .
RUN . /opt/venv/bin/activate && pip install --upgrade pip setuptools wheel && pip install -r requirements.txt

# Run the application:
#u flag if from the prints
COPY ./server/* ./server/
CMD ["python","-u", "server/main.py"]