import json
import zmq
import os


def server(ipc_pipe, host, pid):
    context = zmq.Context()
    socket_interpreter = context.socket(zmq.PAIR)
    socket_interpreter.bind(ipc_pipe)
    print("[ SERVER ] Running in pipe: " + ipc_pipe)

    socket_client = context.socket(zmq.PAIR)
    socket_client.bind(host)
    print("[ SERVER ] Running in host: " + host)

    poller = zmq.Poller()
    poller.register(socket_interpreter, zmq.POLLIN)
    poller.register(socket_client, zmq.POLLIN)

    while True:
        socks = dict(poller.poll())

        if socket_interpreter in socks and socks[socket_interpreter] == zmq.POLLIN:
            message = socket_interpreter.recv_json()
            print("[ SERVER ] Message received from interpreter: " + str(message))
            socket_client.send(str.encode(json.dumps(message)))

        if socket_client in socks and socks[socket_client] == zmq.POLLIN:
            message = socket_client.recv()
            message = message.decode()
            print("[ SERVER ] Message received from client: " + str(message))
            if message == "KILL":
                socket_client.send(str.encode("KILLED!"))
                context.destroy()
                os.system("kill " + str(pid))
            elif message == "HEARTBEAT":
                socket_client.send(str.encode("BEAT"))
            else:
                try:
                    message_json = json.loads(message)
                    socket_interpreter.send_json(message_json)
                except ValueError:
                    socket_client.send(str.encode("Error in your call: "+message))
