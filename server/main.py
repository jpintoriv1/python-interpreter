from multiprocessing import Process
from interpreter import interpreter
from server import server
import socket
import os

HOST = socket.gethostbyname(socket.gethostname())
PORT_SERVER = 5555
HOST_SERVER = "tcp://" + HOST + ":" + str(PORT_SERVER)
f = open("../client/host", "w+")
f.write(HOST_SERVER)
f.close()

ipc_pipe = "ipc://connection.pipe"

server_process = Process(target=server, args=(ipc_pipe, HOST_SERVER, os.getpid()))
server_process.start()

interpreter_process = Process(target=interpreter, args=(ipc_pipe,))
interpreter_process.start()
