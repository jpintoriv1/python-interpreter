from contextlib import redirect_stdout
from io import StringIO
import traceback
import zmq
import ast
import jedi


MODULE_BLACKLIST = ["os", "sys"]


def is_number(string):
    try:
        a = float(string)
        return True, a
    except ValueError:
        return False, 0


def get_type(string):
    number = is_number(string)
    if number[0]:
        if number[1].is_integer():
            return int
        else:
            return float
    elif string == "":
        return None
    else:
        try:
            type_ = type(ast.literal_eval(string))
            return type_
        except ValueError:
            return str


def interpreter(ipc_pipe):
    context = zmq.Context()
    socket = context.socket(zmq.PAIR)
    socket.connect(ipc_pipe)
    print("[ INTERPRETER ] Connect in pipe: " + ipc_pipe)

    exec_no = 0
    globals_params = locals()
    locals_params = {}

    while True:
        message = socket.recv_json()
        if message["channel"] == "RUN":
            print("[ INTERPRETER ] Message received: " + str(message))
            code = message["payload"]["code"]

            exec_no += 1

            response = {
                "channel": "EXECUTED",
                "payload": {
                    "result": [],
                    "result_type": [],
                    "exec_no": [exec_no]
                }
            }
            error = False

            f = StringIO()
            with redirect_stdout(f):
                try:
                    if "import" in code:
                        for module_ in MODULE_BLACKLIST:
                            if module_ in code:
                                raise ValueError('Error in import \n Not permitted import module '+module_)

                    code_ = compile(code, '<string>', 'exec')
                    exec(code_, globals_params, locals_params)
                except Exception as e:
                    # GET THE LAST 3 LINES OF TRACEBACK
                    traceback_ = traceback.format_exc().split("\n")[-3:]
                    error = True
                    print(e)
            result = f.getvalue().strip()

            if not error:
                results = result.split("\n")
                response["payload"]["result"] = results

                types = [None] * len(results)
                i = 0
                for result in results:
                    types[i] = str(get_type(result))
                    i += 1
                response["payload"]["result_type"] = types

            else:
                response["payload"]["traceback"] = traceback_
                response["payload"]["result"] = [result]
                response["payload"]["result_type"] = ["error"]

            socket.send_json(response)

        elif message["channel"] == "INTROSPECTION":
            print("[ INTERPRETER ] Message received: " + str(message))
            query = message["payload"]["query"]
            if "limit" in message["payload"]:
                limit = message["payload"]["limit"]
            else:
                limit = 20

            namespace = [globals_params, locals_params]
            script = jedi.Interpreter(query, namespace)
            completes = script.complete()[:limit]
            results = [value.complete for value in completes]

            response = {
                "channel": "INTROSPECTION",
                "payload": {
                    "query": query,
                    "result": results,
                }
            }

            socket.send_json(response)
