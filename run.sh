if docker container ls | grep interpreter-py; then
	docker restart interpreter-py
else
	docker network create nw-interpreter-py --subnet 172.18.10.0/16 --gateway 172.18.10.1

	docker build --tag interpreter-py:v1 .
	docker run -d --network nw-interpreter-py \
		--ip 172.18.10.2 --name  interpreter-py interpreter-py:v1
fi